/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (800, 400);
    
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");

    messageTypeComboBox.addItem("Note", 1);
    messageTypeComboBox.addItem("Control", 2);
    messageTypeComboBox.addItem("Program", 3);
    channelSlider.setSliderStyle(Slider::IncDecButtons);
    numberSlider.setSliderStyle(Slider::IncDecButtons);
    velocitySlider.setSliderStyle(Slider::IncDecButtons);
    sendButton.setButtonText("Send");
    
    addAndMakeVisible(channelSlider);
    addAndMakeVisible(numberSlider);
    addAndMakeVisible(velocitySlider);
    addAndMakeVisible(messageTypeComboBox);
    addAndMakeVisible(sendButton);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    
    
    messageTypeComboBox.setBounds(10, 10, ((getWidth()-20)/5), 20);
    channelSlider.setBounds((10+(1*(getWidth()-20)/5)), 10, ((getWidth()-20)/5), 20);
    numberSlider.setBounds((10+(2*(getWidth()-20)/5)), 10, ((getWidth()-20)/5), 20);
    velocitySlider.setBounds((10+(3*(getWidth()-20)/5)), 10, ((getWidth()-20)/5), 20);
    sendButton.setBounds((10+(4*(getWidth()-20)/5)), 10, ((getWidth()-20)/5), 20);
    
//    midiComboBox.setBounds(10, 10, ((getWidth()-20)/5), 20);
//    midiLabel1.setBounds(10, 10, getWidth()-20, 20);

}



void MainComponent::handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message)
{
    DBG("midi message received");
    
    String midiText;
    
//    if(message.isNoteOnOrOff())
//    {
//        midiText << "NoteOn : Channel " << message.getChannel();
//        midiText << " : Number " << message.getNoteNumber();
//        midiText << " : Velocity " << message.getVelocity();
//    }
//    if(message.isController())
//    {
//        midiText << "NoteOn : Channel " << message.getChannel();
//        midiText << " : Number " << message.getControllerNumber();
//        midiText << " : Value " << message.getControllerValue();
//    }
//    if (message.isPitchWheel())
//    {
//        midiText << "NoteOn : Channel " << message.getChannel();
//        midiText << " : Number " << message.getNoteNumber();
//        midiText << " : PitchWheel " << message.getPitchWheelValue();
//    }
    
//    midiLabel1.getTextValue() = midiText;
    
    
  //  message.setChannel(channelSlider.getValue());
    
    
    
    
    
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow (message);
}




void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    
}


void MainComponent::buttonClicked (Button*)
{
    
}


