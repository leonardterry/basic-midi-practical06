/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback,
                        public ComboBox::Listener,
                        public Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage&) override;

    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;

    void buttonClicked (Button*) override;
    

private:
    AudioDeviceManager audioDeviceManager;
    ComboBox messageTypeComboBox;
    Slider channelSlider;
    Slider numberSlider;
    Slider velocitySlider;
    TextButton sendButton;
    

    
    
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
